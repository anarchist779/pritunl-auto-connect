module pritunl-auto-connect

go 1.21.5

require github.com/pquerna/otp v1.4.0

require go.uber.org/multierr v1.10.0 // indirect

require (
	github.com/boombuler/barcode v1.0.1-0.20190219062509-6c824513bacc // indirect
	go.uber.org/zap v1.26.0
)
