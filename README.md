# pritunl-auto-connect

## Программа-клиент к `Pritunl` для автоматического подключения к профилям `VPN`, которые требуют `PIN` и `OTP` коды

## Аргументы запуска:
1. `--totp-secret`- Секрет для генерации `OTP`-кодов, к примеру `ZVBFRGLSXLOPMNB4`

2. `--client-path`- Путь к программе `pritunl-client`, к примеру `/mnt/c/Program Files (x86)/Pritunl/pritunl-client.exe`

3. `--profile-path`- Путь к `tar` или `zip` профилю `pritunl`, к примеру `/mnt/c/Program Files (x86)/Pritunl/Profiles/example.tar`

4. `--profile-pin`- `PIN` запрашиваемый `Pritunl` при подключении, к примеру `6545781455890269`

5. `--profile-name`- Название провиля `Pritunl`, можно получить путём импорта в `Pritunl` с помощью команды:
    ```shell
    "/mnt/c/Program Files (x86)/Pritunl/pritunl-client.exe" add "/mnt/c/Users/Anarchist/Desktop/example.tar"
    ```
    А далее уже можно будет узнать имя в списке, который можно получить через команду, в таблице будет свойство `NAME`, к примеру `example (xfa-development)`:
    ```shell
    "/mnt/c/Program Files (x86)/Pritunl/pritunl-client.exe" list
    ```
## Способ запуска в Windows:
1. `PowerShell`:
```powershell
. "$PSScriptRoot\pritunl-auto-connect.exe" `
    --totp-secret="ZVBFRGLSXLOPMNB4" `
    --client-path="${env:ProgramFiles(x86)}\Pritunl\pritunl-client.exe" `
    --profile-path="$PSScriptRoot\example.tar" `
    --profile-pin="6545781455890269" `
    --profile-name="example (xfa-development)"
```
2. `Batch`:
```bat
"%CD%\pritunl-auto-connect.exe" ^
    --totp-secret="ZVBFRGLSXLOPMNB4" ^
    --client-path="%PROGRAMFILES(x86)%\Pritunl\pritunl-client.exe" ^
    --profile-path="%CD%\example.tar" ^
    --profile-pin="6545781455890269" ^
    --profile-name="example (xfa-development)"
```