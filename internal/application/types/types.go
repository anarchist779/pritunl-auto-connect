package types

import "strconv"

type ProfileState string

const (
	ProfileStateEnabled  ProfileState = "enabled"
	ProfileStateDisabled ProfileState = "disabled"
)

type ProfileStatus string

const (
	ProfileStatusDisconnected ProfileStatus = "disconnected"
	ProfileStatusConnecting   ProfileStatus = "connecting"
)

type Bool bool

func (b Bool) String() string {
	return strconv.FormatBool(bool(b))
}
