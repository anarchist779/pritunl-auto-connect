package client

import "pritunl-auto-connect/internal/application/types"

type ProfileState string

func (s ProfileState) Type() (v types.ProfileState) {
	switch s {
	case "Enabled":
		v = types.ProfileStateEnabled
	default:
		v = types.ProfileStateDisabled
	}

	return
}

type ProfileStatus string

func (s ProfileStatus) Type() (v types.ProfileStatus) {
	switch s {
	case "Connecting":
		v = types.ProfileStatusConnecting
	case "Disconnected":
		v = types.ProfileStatusDisconnected
	}

	return
}
