package client

type Profile struct {
	Id            string        `json:"id"`
	Name          string        `json:"name"`
	State         ProfileState  `json:"state"`
	Connected     bool          `json:"connected"`
	Status        ProfileStatus `json:"status"`
	ServerAddress string        `json:"server_address"`
	ClientAddress string        `json:"client_address"`
}
