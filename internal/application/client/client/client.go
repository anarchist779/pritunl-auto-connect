package client

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"pritunl-auto-connect/internal/application/entities"
	"pritunl-auto-connect/internal/application/interfaces"
	"pritunl-auto-connect/internal/application/types"
	"time"
)

type Client struct {
	Log  interfaces.Logger
	Code interfaces.Code
	Path string
}

func (c *Client) Profiles(ctx context.Context) (p map[string]entities.Profile, err error) {
	b, err := exec.CommandContext(ctx, c.Path, "list", "-j").CombinedOutput()

	if err != nil {
		return p, Error{b, fmt.Sprintf("err get profiles: %s", err)}
	}

	var o []Profile

	if err := json.Unmarshal(b, &o); err != nil {
		return p, Error{Message: fmt.Sprintf("err unmarshal profiles: %s", err)}
	}

	p = make(map[string]entities.Profile, len(o))

	for i := range o {
		p[o[i].Name] = entities.Profile{
			Id:            o[i].Id,
			Name:          o[i].Name,
			State:         o[i].State.Type(),
			Connected:     types.Bool(o[i].Connected),
			Status:        o[i].Status.Type(),
			ServerAddress: o[i].ServerAddress,
			ClientAddress: o[i].ClientAddress,
		}
	}

	return p, nil
}

func (c *Client) Profile(ctx context.Context, e entities.Profile) (p entities.Profile, err error) {
	o, err := c.Profiles(ctx)

	if err != nil {
		return p, Error{Message: fmt.Sprintf("err get profile %s: %s", e.Name, err)}
	}

	p, ok := o[e.Name]

	if !ok {
		err = Error{Message: fmt.Sprintf("err non-existent profile %s", e.Name)}
	}

	return
}

func (c *Client) Connect(ctx context.Context, e entities.Profile) (err error) {
	v, err := c.Profile(ctx, e)

	if err != nil {
		return
	}

	code, err := c.Code.Generate()

	if err != nil {
		return Error{Message: fmt.Sprintf("err code generate: %s", err)}
	}

	cmd := exec.CommandContext(ctx, c.Path, "start", v.Id, "-p", e.Pin+code)

	if b, err := cmd.CombinedOutput(); err != nil {
		return Error{Body: b, Message: fmt.Sprintf("err start: %s", err)}
	}

	return
}

func (c *Client) Disconnect(ctx context.Context, e entities.Profile) (err error) {
	v, err := c.Profile(ctx, e)

	if err != nil {
		return
	}

	if b, err := exec.CommandContext(ctx, c.Path, "stop", v.Id).CombinedOutput(); err != nil {
		return Error{Body: b, Message: fmt.Sprintf("err disconnect profile %s: %s", e.Name, err)}
	}

	return
}

func (c *Client) Import(ctx context.Context, e entities.Profile) (err error) {
	if b, err := exec.CommandContext(ctx, c.Path, "add", e.Path).CombinedOutput(); err != nil {
		return Error{Body: b, Message: fmt.Sprintf("err import profile %s: %s", e.Name, err)}
	}

	return
}

func (c *Client) Watch(ctx context.Context, e entities.Profile) error {
	t := time.NewTicker(time.Second * 4)

	c.Log.Info("watch profiles")

	for {
		select {
		case <-ctx.Done():
			t.Stop()

			c.Log.Infof("disconnect %s", e)

			ctx, cancel := context.WithCancel(context.WithoutCancel(ctx))

			if err := c.Disconnect(ctx, e); err != nil {
				c.Log.Error(err.Error())
			}

			cancel()

			return nil
		case <-t.C:
			v, err := c.Profile(ctx, e)

			if err != nil {
				c.Log.Error(err.Error())

				c.Log.Infof("import profile: %s", e)

				if err := c.Import(ctx, e); err != nil {
					c.Log.Error(err.Error())
				}

				continue
			}

			switch {
			case v.IsConnected():
				continue
			case v.Status == types.ProfileStatusConnecting:
				c.Log.Infof("connecting profile: %s", v)
			case v.Status == types.ProfileStatusDisconnected:
				c.Log.Infof("disconnect profile: %s", v)

				c.Log.Infof("connect profile: %s", v)

				if err := c.Connect(ctx, e); err != nil {
					c.Log.Error(err.Error())
				}
			}
		}
	}
}
