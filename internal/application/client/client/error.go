package client

import "fmt"

type Error struct {
	Body    []byte
	Message string
}

func (err Error) Error() (s string) {
	if len(err.Body) > 0 {
		return fmt.Sprintf("err default client: %s: %s", err.Message, string(err.Body))
	}

	return fmt.Sprintf("err default client: %s", err.Message)
}
