package client

import (
	"pritunl-auto-connect/internal/application/client/client"
	"pritunl-auto-connect/internal/application/interfaces"
)

func New(code interfaces.Code, configuration interfaces.Configuration, log interfaces.Logger) interfaces.Client {
	return &client.Client{
		Log:  log,
		Code: code,
		Path: configuration.ClientPath(),
	}
}
