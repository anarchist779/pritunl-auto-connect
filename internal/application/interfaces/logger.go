package interfaces

type Logger interface {
	Errorf(string, ...any)
	Infof(string, ...any)
	Info(string)
	Error(string)
}
