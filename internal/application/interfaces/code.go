package interfaces

type Code interface {
	Generate() (string, error)
}
