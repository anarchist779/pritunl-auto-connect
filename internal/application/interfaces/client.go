package interfaces

import (
	"context"
	"pritunl-auto-connect/internal/application/entities"
)

type Client interface {
	Import(context.Context, entities.Profile) error
	Watch(context.Context, entities.Profile) error
	Profiles(context.Context) (map[string]entities.Profile, error)
	Profile(context.Context, entities.Profile) (entities.Profile, error)
	Connect(context.Context, entities.Profile) error
	Disconnect(context.Context, entities.Profile) error
}
