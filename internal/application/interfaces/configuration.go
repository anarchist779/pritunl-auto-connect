package interfaces

type Configuration interface {
	ClientPath() string
	ProfileName() string
	ProfilePath() string
	ProfilePin() string
	CodeSecret() string
}
