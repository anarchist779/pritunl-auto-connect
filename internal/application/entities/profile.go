package entities

import (
	"fmt"
	"pritunl-auto-connect/internal/application/types"
	"reflect"
	"strings"
	"time"
)

type Profile struct {
	Id            string              `name:"id"`
	Pin           string              `name:"pin"`
	Name          string              `name:"name"`
	Path          string              `name:"path"`
	State         types.ProfileState  `name:"state"`
	Connected     types.Bool          `name:"connected"`
	Uptime        time.Duration       `name:"uptime"`
	Status        types.ProfileStatus `name:"status"`
	ServerAddress string              `name:"server_address"`
	ClientAddress string              `name:"client_address"`
}

func (s Profile) String() string {
	f := []string{}

	value := reflect.ValueOf(s)
	t := value.Type()

	for i := 0; i < value.NumField(); i++ {
		v := value.Field(i)

		if !v.IsZero() {
			f = append(f, fmt.Sprintf("%s: %s", t.Field(i).Tag.Get("name"), v.Interface()))
		}
	}

	j := strings.Join(f, ", ")

	return fmt.Sprintf("{ %s }", j)
}

func (p *Profile) IsConnected() (b bool) {
	if !p.Connected {
		return
	}

	if p.Status == types.ProfileStatusDisconnected {
		return
	}

	if p.Status == types.ProfileStatusConnecting {
		return
	}

	return len(p.ClientAddress) > 0 && len(p.ServerAddress) > 0
}
