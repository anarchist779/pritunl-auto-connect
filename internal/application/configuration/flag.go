package configuration

import (
	"pritunl-auto-connect/internal/application/configuration/flag"
	"pritunl-auto-connect/internal/application/interfaces"
)

func New() (interfaces.Configuration, error) {
	return flag.New()
}
