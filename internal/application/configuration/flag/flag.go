package flag

import (
	"flag"
)

type Flag struct {
	Arguments struct {
		TOTPSecret Text
		ClientPath Text
		Profile    struct {
			Pin  Text
			Name Text
			Path Text
		}
	}
}

func New() (f Flag, err error) {
	flag.TextVar(&f.Arguments.ClientPath, "client-path", EmptyText, "Pritunl client path")

	flag.TextVar(&f.Arguments.TOTPSecret, "totp-secret", EmptyText, "TOTP secret key")

	flag.TextVar(&f.Arguments.Profile.Path, "profile-path", EmptyText, "Pritunl profile path or URI")

	flag.TextVar(&f.Arguments.Profile.Name, "profile-name", EmptyText, "Pritunl profile name")

	flag.TextVar(&f.Arguments.Profile.Pin, "profile-pin", EmptyText, "Pritunl profile pin")

	flag.Parse()

	if f.IsZero() {
		flag.Usage()

		err = flag.ErrHelp
	}

	return
}

func (a Flag) ClientPath() string {
	return a.Arguments.ClientPath.String()
}

func (a Flag) ProfilePath() string {
	return a.Arguments.Profile.Path.String()
}

func (a Flag) ProfilePin() string {
	return a.Arguments.Profile.Pin.String()
}

func (a Flag) CodeSecret() string {
	return a.Arguments.TOTPSecret.String()
}

func (a Flag) ProfileName() string {
	return a.Arguments.Profile.Name.String()
}

func (a Flag) IsZero() (b bool) {
	for _, v := range []Text{
		a.Arguments.ClientPath,
		a.Arguments.TOTPSecret,
		a.Arguments.Profile.Pin,
		a.Arguments.Profile.Name,
		a.Arguments.Profile.Path,
	} {
		if len(v) == 0 {
			return true
		}
	}

	return
}
