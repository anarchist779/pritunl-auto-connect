package flag

import (
	"strings"
)

type Text string

const EmptyText Text = ""

func (t Text) MarshalText() (text []byte, err error) {
	text = []byte(t)

	return
}

func (t *Text) UnmarshalText(text []byte) (err error) {
	s := strings.Trim(string(text), "\"")

	*t = Text(s)

	return
}

func (t Text) String() string {
	return string(t)
}
