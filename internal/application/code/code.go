package code

import "pritunl-auto-connect/internal/application/interfaces"

func New(c interfaces.Configuration) interfaces.Code {
	return TOTP{c.CodeSecret()}
}
