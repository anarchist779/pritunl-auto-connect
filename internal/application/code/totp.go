package code

import (
	"time"

	"github.com/pquerna/otp/totp"
)

type TOTP struct {
	Secret string
}

func (t TOTP) Generate() (string, error) {
	return totp.GenerateCode(t.Secret, time.Now())
}
