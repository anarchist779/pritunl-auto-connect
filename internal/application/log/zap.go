package log

import (
	"fmt"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Zap struct{ *zap.Logger }

func (z Zap) Errorf(f string, a ...any) {
	z.Logger.Error(fmt.Sprintf(f, a...))
}

func (z Zap) Infof(f string, a ...any) {
	z.Logger.Info(fmt.Sprintf(f, a...))
}

func (z Zap) Info(s string) {
	z.Logger.Info(s)
}

func (z Zap) Error(s string) {
	z.Logger.Error(s)
}

func NewZap() (Zap, error) {
	c := zap.NewProductionConfig()

	c.EncoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.DateTime)

	c.EncoderConfig.EncodeDuration = zapcore.SecondsDurationEncoder

	c.DisableStacktrace = true

	c.DisableCaller = true

	z, err := c.Build(zap.AddCallerSkip(1))

	return Zap{z}, err
}
