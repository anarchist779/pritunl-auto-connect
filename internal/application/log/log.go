package log

import "pritunl-auto-connect/internal/application/interfaces"

func New() (l interfaces.Logger, err error) {
	l, err = NewZap()

	if err != nil {
		err = Error(err.Error())
	}

	return
}
