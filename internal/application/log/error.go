package log

import "fmt"

type Error string

func (err Error) Error() string {
	return fmt.Sprintf("err create logger: %s", string(err))
}
