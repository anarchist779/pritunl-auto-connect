package main

import (
	"context"
	"os"
	"os/signal"
	"pritunl-auto-connect/internal/application/client"
	"pritunl-auto-connect/internal/application/code"
	"pritunl-auto-connect/internal/application/configuration"
	"pritunl-auto-connect/internal/application/entities"
	"pritunl-auto-connect/internal/application/log"
	"syscall"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	configuration, err := configuration.New()

	if err != nil {
		panic(err)
	}

	log, err := log.New()

	if err != nil {
		panic(err)
	}

	code := code.New(configuration)

	client := client.New(code, configuration, log)

	go func() {
		e := entities.Profile{
			Pin:  configuration.ProfilePin(),
			Name: configuration.ProfileName(),
			Path: configuration.ProfilePath(),
		}

		if err := client.Watch(ctx, e); err != nil {
			panic(err)
		}
	}()

	s := make(chan os.Signal, 1)

	signal.Notify(s, syscall.SIGINT, syscall.SIGTERM)

	<-s

	cancel()

	time.Sleep(time.Second * 4)
}
